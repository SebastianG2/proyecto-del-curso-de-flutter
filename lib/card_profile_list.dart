import 'package:flutter/cupertino.dart';
import 'package:platzi_trips_app/card_profile.dart';

class CardProfileList extends StatelessWidget{
  const CardProfileList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      scrollDirection: Axis.vertical,
      padding: const EdgeInsets.all(20.0),
      children: [
        CardProfile(0),
        CardProfile(50),
        CardProfile(50),
        CardProfile(50)
      ],
    );
    /*return Container(
      height: 250.0,
      child: ListView(
        padding: const EdgeInsets.all(18.0),
        scrollDirection: Axis.vertical,
        children: [
          CardProfile(),
          CardProfile(),
        ],
      ),
    );*/
  }

}