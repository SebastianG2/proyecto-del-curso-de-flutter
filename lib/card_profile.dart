import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:platzi_trips_app/floating_action_button_green.dart';

class CardProfile extends StatelessWidget{
  String pathImageCard = 'assets/img/1.jpg';
  double topPadding = 50.0;
  CardProfile(this.topPadding, {Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final img = Container(

      height: 250.0,
      width: 378.0,
      margin: EdgeInsets.only(
          top: topPadding,
          bottom: 55,
      ),
      decoration: BoxDecoration(
          image: DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage(pathImageCard)
          ),
          borderRadius: const BorderRadius.all(Radius.circular(22.0)),
          shape: BoxShape.rectangle,
          boxShadow: const <BoxShadow>[
            BoxShadow(
                color: Colors.black38,
                blurRadius: 15.0,
                offset: Offset(0.0, 7.0)
            )
          ]
      ),
    );

    final dataCard = Container(

      width: 320,
      height: 138,
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        margin: const EdgeInsets.only(
          left: 22,
          right: 22,
          top: 25,
          bottom: 10
        ),
        elevation: 12,
        child: Column(
          children: [
            const ListTile(
              contentPadding: EdgeInsets.fromLTRB(15, 10, 25, 0),
              title: Text(
                  'Titulo',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Lato'
                  ),
              ),
              subtitle: Text(
                  'Hiking, water fall hunting, natural bath, senery and photography',
                  style: TextStyle(
                    fontSize: 12,
                    color: Colors.grey
                  ),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(
                left: 15,
              ),
              alignment: Alignment.topLeft,
              child: const Text(
                  'Steps 123, 123, 123',
                  style: TextStyle(
                    color: Colors.orange,
                    fontWeight: FontWeight.bold,
                    fontSize: 16
                  ),
              ),
            )

          ],
        ),
      )
    );

    final imgAndCard = Stack(
      alignment: Alignment.bottomCenter,
      children: [
        img,
        dataCard
      ],
    );

    return Stack(
      alignment: Alignment.bottomRight,
      children: [
        imgAndCard,
        const Positioned(
          right: 30,
          child: FloatingActionButtonGreen()
        )
      ],

    );
  }

}