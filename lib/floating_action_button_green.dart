import 'package:flutter/material.dart';

class FloatingActionButtonGreen extends StatefulWidget{
  const FloatingActionButtonGreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {

    return _FloatingActionButtonGreen();
  }

}

class _FloatingActionButtonGreen extends State<FloatingActionButtonGreen> {
  bool selected = false;
  void onPressedFav(){
      setState((){
        selected = !selected;
      });
  }
  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      backgroundColor: const Color(0xFF11DA53),
      mini: true,
      tooltip: 'Fav',
      onPressed: onPressedFav,
      child: Icon( selected ? Icons.favorite_border : Icons.favorite),
    );
  }
  
}