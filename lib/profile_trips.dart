import 'package:flutter/material.dart';
import 'package:platzi_trips_app/card_profile_list.dart';

class ProfileTrips extends StatelessWidget{
  const ProfileTrips({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(

      children: [
        const GradientBackProfile(),
        Container(
            margin: const EdgeInsets.only(
              top: 300,
            ),
            width: 460.0,
            child: const CardProfileList()
        )
      ],
    );
  }

}







//GradientBackProfile
class GradientBackProfile extends StatefulWidget{

  const GradientBackProfile({Key? key}) : super(key: key);

  @override
  State<GradientBackProfile> createState() => _GradientBackProfileState();
}

class _GradientBackProfileState extends State<GradientBackProfile> {
  String title = 'Profile';

  String name = 'Sebas Garcia';

  String pathImage = "assets/img/perfil_egel.jpg";

  String pathImageCard = 'assets/img/1.jpg';

  @override
  Widget build(BuildContext context) {
    //Contenedor con el titulo
    final headerTitle = Container(
        margin: const EdgeInsets.only(
            left: 20.0
        ),
        child: Text(
          title,
          style: const TextStyle(
              color: Colors.white,
              fontSize: 30.0,
              fontFamily: "Lato",
              fontWeight: FontWeight.bold
          ),
        )
    );

    //Contenedor con el icono
    final iconSettings = Container(
      margin: const EdgeInsets.only(
        right: 20
      ),
      child: const Icon(
        Icons.settings,
        color: Colors.white
      )
    );

    //Primera fila header
    final titleIcon = Container(
      margin: const EdgeInsets.only(
        top:53
      ),
      child: Row(
        children: [
          headerTitle,
          const Spacer(),
          iconSettings
        ],
      )

    );


    final userName = Container (
      margin: const EdgeInsets.only(
        top: 20.0,
        left: 10.0,
      ),
      child: Text(
        name,
        style: const TextStyle(
          fontFamily: 'Lato',
          fontSize: 18.0,
          color: Colors.white
        ),
      ),
    );

    final userMail = Container(
      margin: const EdgeInsets.only(
          left: 10.0
      ),
      child: const Text(
        'andres.sebastian@mail.com',
        textAlign: TextAlign.left,
        style: TextStyle(
            fontFamily: "Lato",
            fontSize: 13.0,
            color: Color(0xFFa3a5a7)
        ),
      ),
    );

    final userDetails = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        userName,
        userMail
      ],
    );

    final photo = Container(
        margin: const EdgeInsets.only(
          top: 20.0,
          left: 28.0,
          right: 10.0
        ),
        width: 90.0,
        height: 90.0,
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage(pathImage)
            )
        )
    );


    final userRow = Row(
      children: [
        photo,
        userDetails
      ],
    );

    final iconBookmark = Container(
      margin: const EdgeInsets.only(
          right: 10
      ),
      child: ElevatedButton(
        onPressed: () {},

        child: const Icon(
            Icons.bookmark_border_outlined,
            color: Colors.blue,
            size: 20
        ),
        style: ButtonStyle(
          shape: MaterialStateProperty.all(const CircleBorder()),
          padding: MaterialStateProperty.all(const EdgeInsets.all(9)),
          backgroundColor: MaterialStateProperty.all(Colors.white), // <-- Button color
          overlayColor: MaterialStateProperty.resolveWith<Color?>((states) {
            if (states.contains(MaterialState.pressed)) return Colors.blue; // <-- Splash color
          }),
        ),
      )
    );

    final iconTv = Container(
        margin: const EdgeInsets.only(
            right: 00
        ),
        child: ElevatedButton(
          onPressed: () {},

          child: const Icon(
              Icons.tablet_mac_rounded,
              color: Colors.blue,
              size: 20
          ),
          style: ButtonStyle(
            shape: MaterialStateProperty.all(const CircleBorder()),
            padding: MaterialStateProperty.all(const EdgeInsets.all(9)),
            backgroundColor: MaterialStateProperty.all(Colors.white), // <-- Button color
            overlayColor: MaterialStateProperty.resolveWith<Color?>((states) {
              if (states.contains(MaterialState.pressed)) return Colors.blue; // <-- Splash color
            }),
          ),
        )
    );

    final iconAdd = Container(
        margin: const EdgeInsets.only(
            left:16,
            right: 16
        ),
        child: ElevatedButton(
          onPressed: () {},

          child: const Icon(
              Icons.add,
              color: Colors.blue,
              size: 45
          ),
          style: ButtonStyle(
            shape: MaterialStateProperty.all(const CircleBorder()),
            padding: MaterialStateProperty.all(const EdgeInsets.all(10)),
            backgroundColor: MaterialStateProperty.all(Colors.white), // <-- Button color
            overlayColor: MaterialStateProperty.resolveWith<Color?>((states) {
              if (states.contains(MaterialState.pressed)) return Colors.blue; // <-- Splash color
            }),
          ),
        )
    );

    final iconMail = Container(
        margin: const EdgeInsets.only(
            right: 0
        ),
        child: ElevatedButton(
          onPressed: () {},

          child: const Icon(
              Icons.mail_outline_rounded,
              color: Colors.blue,
              size: 20
          ),
          style: ButtonStyle(
            shape: MaterialStateProperty.all(const CircleBorder()),
            padding: MaterialStateProperty.all(const EdgeInsets.all(9)),
            backgroundColor: MaterialStateProperty.all(Colors.white), // <-- Button color
            overlayColor: MaterialStateProperty.resolveWith<Color?>((states) {
              if (states.contains(MaterialState.pressed)) return Colors.blue; // <-- Splash color
            }),
          ),
        )
    );

    final iconUser = Container(
        margin: const EdgeInsets.only(
            left: 10
        ),
        child: ElevatedButton(
          onPressed: null,

          child: const Icon(
              Icons.person,
              color: Colors.blue,
              size: 20
          ),
          style: ButtonStyle(
            shape: MaterialStateProperty.all( const CircleBorder()),
            padding: MaterialStateProperty.all(const EdgeInsets.all(9)),
            backgroundColor: MaterialStateProperty.all(Colors.white), // <-- Button color
            overlayColor: MaterialStateProperty.resolveWith<Color?>((states) {
              if (states.contains(MaterialState.pressed)) return Colors.blue; // <-- Splash color
            }),
          ),
        )
    );


    final actions = Container(
      margin: const EdgeInsets.only(
          top: 18
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          iconBookmark,
          iconTv,
          iconAdd,
          iconMail,
          iconUser
        ],
      )
    );



    final allData = Container(
      height: 400.0,
      decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color(0xFF4268D3),
              Color(0xFF584CD1)
            ],
            begin: FractionalOffset(0.2, 0.0),
            end: FractionalOffset(1.0, 0.6),
            stops: [0.0, 0.6],
            tileMode: TileMode.clamp,
          )
      ),
      child: Column(
        children: [
          titleIcon,
          userRow,
          actions
        ],
      ),
      alignment: const Alignment(1.0, 1.0),

    );



    return allData;
  }
}